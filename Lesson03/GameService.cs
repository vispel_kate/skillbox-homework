using System;

namespace Lesson03
{
    public class GameService
    {
        public static void StartNewGame()
        {
            Console.WriteLine("Начало новой игры в числа. Введите количество игроков");
            string userInput = Console.ReadLine();
            uint users = UInt32.Parse(userInput);

            if (users < 2)
            {
                StartNewSingleGame();
            }
            else
            {
                StartNewMultiPlayerGame(users);   
            }
        }

        private static void StartNewSingleGame()
        {
            Console.WriteLine($"Введите никнейм для пользователя");
            string username = Console.ReadLine();
            int gameNumber = GenerateNumber(12, 120);
            int winnerIndex = 0;
            do
            {
                for (int i = 0; i < 2; i++)
                {
                    Console.WriteLine($"Число: {gameNumber}");
                    if (i != 1)
                    {
                        try
                        {
                            var userTry = ReadUserTry(username);
                            gameNumber = gameNumber - userTry;
                            winnerIndex = i;
                        }  catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                    else
                    {
                        int computerTry = GenerateNumber(1, 5);
                        Console.WriteLine($"Число загаданное компьютером {computerTry}");
                        if (computerTry > gameNumber)
                        {
                            computerTry = gameNumber;
                        }
                        gameNumber = gameNumber - computerTry;
                        winnerIndex = i;
                    }
                    
                    if (IsEndOfGame(gameNumber)) break;
                }
            } while (gameNumber > 0);

            string winner = winnerIndex == 0 ? username : "компьютер";
            Console.WriteLine($"Победил {winner}");
        }

        private static bool IsEndOfGame(int gameNumber)
        {
            return gameNumber == 0;
        }

        private static void StartNewMultiPlayerGame(uint users)
        {
            var userNames = GetUserNames(users);
            var gameNumber = GenerateNumber(12, 120);
            var winnerIndex = 0;
            do
            {
                for (var i = 0; i < users; i++)
                {
                    try
                    {
                        gameNumber = CompleteGameTurn(gameNumber, userNames[i]);
                        if (gameNumber == 0)
                        {
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine("Пожалуйста, попробуйте ещё раз.");
                        i--;
                    }
                }
            } while (gameNumber > 0);

            Console.WriteLine($"Победил {userNames[winnerIndex]}");
        }

        private static int CompleteGameTurn(int gameNumber, string userName)
        {
            Console.WriteLine($"Число: {gameNumber}");
            var userTry = ReadUserTry(userName);
            if (gameNumber < userTry)
            {
                throw new InvalidOperationException("Число пользователя не может быть больше заданного игрой числа");
            }
            gameNumber = gameNumber - userTry;
            return gameNumber;
        }

        private static int ReadUserTry(string userName)
        {
            Console.WriteLine($"Ход {userName}. Введите число: ");
            var userTryInput = Console.ReadLine();
            var userTry = int.Parse(userTryInput ?? throw new InvalidOperationException("Не может быть пустым"));
            if (userTry < 1 || userTry > 4)
            {
                throw new InvalidOperationException("Число не может быть меньше/больше заданного диапозона");
            }

            return userTry;
        }

        private static string[] GetUserNames(uint users)
        {
            var userNames = new string[users];
            for (var i = 0; i < users; i++)
            {
                Console.WriteLine($"Введите никнейм для {i + 1} пользователя");
                var username = Console.ReadLine();
                userNames[i] = username;
            }
            return userNames;
        }

        private static int GenerateNumber(byte min, byte max)
        {
            return new Random().Next(min, max);
        }
    }
}