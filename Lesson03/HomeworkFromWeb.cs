using System;

namespace Lesson03
{
    public class HomeworkFromWeb
    {
        public void FirstTask()
        {
            //First homework part
            Console.WriteLine("Определение чётности числа");
            Console.WriteLine("Введите число: ");
            string userNumberInput = Console.ReadLine();
            int userNumber = Int32.Parse(userNumberInput);

            if(userNumber % 2 == 0)
            {
                Console.WriteLine("Число чётное");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Число нечётное");
                Console.ReadKey();
            }
        }

        public void SecondTask()
        {
            //Second homework part
            Console.WriteLine("Определение суммы номинала карт на руках у пользователя");
            Console.WriteLine("Здравствуйте, введите количество ваших карт: ");
            string cardsNumberInput = Console.ReadLine();
            byte cardsNumber = Byte.Parse(cardsNumberInput);
            
            byte cardsWeight = 0;
            for (int i = 1; i <= cardsNumber; i++)
            {
                Console.WriteLine($"Введите номинал {i} карты : ");
                string cardDenominationInput = Console.ReadLine();
                switch (cardDenominationInput)
                {
                    case "6":
                        cardsWeight += 6;
                        break;
                    case "7":
                        cardsWeight += 7;
                        break;
                    case "8":
                        cardsWeight += 8;
                        break;
                    case "9":
                        cardsWeight += 9;
                        break;
                    case "10":
                        cardsWeight += 10;
                        break;
                    case "J":
                        cardsWeight += 10;
                        break;
                    case "Q":
                        cardsWeight += 10;
                        break;
                    case "K":
                        cardsWeight += 10;
                        break;
                    case "T":
                        cardsWeight += 10;
                        break;
                    default:
                        i--;
                        Console.WriteLine($"Введен неправильных номинал карты. {cardDenominationInput} не существует. Попробуйте ещё раз. ");
                        break;
                        //Here should be
                        //throw new ArgumentException(message: $"Unexpected card denomination: {cardDenominationInput}");
                }
            }
            Console.WriteLine($"Сумма номиналов ваших карт : {cardsWeight} ");
        }

        public void ThirdTask()
        {
            Console.WriteLine("Определение является ли число простым или нет");
            Console.WriteLine("Введите число: ");
            string userInput = Console.ReadLine();
            uint number = UInt32.Parse(userInput);

            bool isPrimeNumber = false;
            if (number > 1)
            {
                uint count = 1;
                while (count < number)
                {
                    if (number % count != 0)
                    {
                        isPrimeNumber = true;
                    }
                    count++;
                }
            }

            string isPrimeNumberPrefix = isPrimeNumber ? "" : " не ";
            Console.WriteLine($"{number} {isPrimeNumberPrefix}является простым числом");
            Console.ReadLine();
        }
    }
}