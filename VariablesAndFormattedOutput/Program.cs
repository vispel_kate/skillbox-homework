﻿using System;

namespace VariablesAndFormattedOutput
{
    class Program
    {
        static void Main(string[] args)
        {
            //First homework part 
            string fullName = "Elon Reeve Musk";
            byte age = 50;
            string email = "e.musk@test.com";
            float programmingMark = 10.0f;
            float mathMark = 10.0f;
            float physicMark = 10.0f;
            
            
            string pattern = "Ф.И.О: {0} \n" +
                             "Bозраст: {1} \n" +
                             "Email: {2} : \n" +
                             "Оценка по программированию {3:F1} \n" +
                             "Оценка по матиматике: {4:F1}\n" +
                             "Оценка по физике: {5:F1}\n";
            
            Console.WriteLine(pattern,
                fullName,
                age,
                email,
                programmingMark,
                mathMark,
                physicMark
            );
            
            Console.ReadKey();

            string format = $"Ф.И.О: {fullName}\n" +
                         $"Bозраст: {age}\n" +
                         $"Email: {email}\n" +
                         $"Оценка по программированию: {programmingMark:F1}\n" +
                         $"Оценка по матиматике: {mathMark:F1}\n" +
                         $"Оценка по физике: {physicMark:F1}";
            
            Console.WriteLine(format);
            Console.ReadKey();
            
            int centerX = (Console.WindowWidth / 2) - (format.Length / 2);
            int centerY = (Console.WindowHeight / 2) - 1;
            Console.SetCursorPosition(centerX, centerY);
            Console.WriteLine(format);
            
            
            // Second homework part
            float sumMarks = programmingMark + mathMark + physicMark;
            float gpaMark = sumMarks / 3;

            Console.WriteLine("Сумма оценок по предметам {0:F1}", sumMarks);
            Console.ReadKey();

            var sumValueOutput = $"Сумма оценок по предметам {sumMarks:F1}";
            Console.WriteLine(sumValueOutput);
            Console.ReadKey();

            centerX = (Console.WindowWidth / 2) - (sumValueOutput.Length / 2);
            Console.SetCursorPosition(centerX, centerY);
            Console.WriteLine(sumValueOutput);
            Console.ReadKey();

            Console.WriteLine("Средний балл: {0:F1}", gpaMark);
            Console.ReadKey();

            string gpaValueOutput = $"Средний балл: {gpaMark:F1}";
            Console.WriteLine(gpaValueOutput);
            Console.ReadKey();
            
            centerX = (Console.WindowWidth / 2) - (gpaValueOutput.Length / 2);
            Console.SetCursorPosition(centerX, centerY);
            Console.WriteLine(gpaValueOutput);
        }
    }
}